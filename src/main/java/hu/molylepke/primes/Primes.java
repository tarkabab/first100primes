package hu.molylepke.primes;

public class Primes {

    public static void main(String[] args) {
        System.out.println("First 100 primes:");
        int[] primes = get(100);
        int i = 1;
        for(int p : primes) {
            System.out.println("" + i++ + " : " + p);
        }
    }

    static int[] get(int maxCount) {
        int[] result = new int[maxCount];

        int maybePrime = 2;
        int numberOfPrimesFound = 0;
        while (numberOfPrimesFound < maxCount) {
            if(isPrime(maybePrime, result, numberOfPrimesFound)) {
                result[numberOfPrimesFound++] = maybePrime;
            }
            maybePrime++;
        }
        return result;
    }

    private static boolean isPrime(int maybePrime, int[] primes, int numberOfPrimes) {
        boolean result = true;

        for (int j = 0; j < numberOfPrimes; j++) {
            if (maybePrime % primes[j] == 0) {
                result = false;
                break;
            }
        }
        return result;
    }
}
