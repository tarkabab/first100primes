package hu.molylepke.primes;

import org.junit.Test;
import static org.junit.Assert.*;

public class PrimesTest {

    @Test
    public void firstPrimeIs2() {
        int[] expected = { 2 };

        assertArrayEquals(expected, Primes.get(1));
    }

    @Test
    public void first5PrimeIs_2_3_5_7_11() {
        int[] expected = { 2, 3, 5, 7, 11 };

        assertArrayEquals(expected, Primes.get(5));
    }
}
